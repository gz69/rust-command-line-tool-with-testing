
# mini_project8_rust_CLI

## Overview
`mini_project8_rust_CLI` is a Rust command-line tool designed for searching specified patterns within text files. This project emphasizes not only on functionality but also on data processing efficiency, comprehensive testing, and clear documentation.

### Features
- Command-line pattern search in text files.
- Efficient data ingestion and processing.
- Extensive unit testing suite.
- Well-documented code and usage instructions.

## Getting Started

### Prerequisites
Ensure you have Rust and Cargo installed on your system. If not, follow the installation instructions on the [official Rust website](https://www.rust-lang.org/tools/install).

### Installation
1. Clone the repository to your local machine:
   ```bash
   git clone https://your-repository-url/mini_project8_rust_CLI.git
   cd mini_project8_rust_CLI
   ```
2. Build the project using Cargo:
   ```bash
   cargo build --release
   ```

### Usage
To search for a pattern within a file, use the following command:
```bash
cargo run --release -- <pattern> <path_to_file>
```
Replace `<pattern>` with the pattern you wish to search for, and `<path_to_file>` with the path to the text file.

## Development

### Adding Dependencies
Add necessary dependencies to your `Cargo.toml` file:
```toml
[dependencies]
clap = { version = "4.0", features = ["derive"] }
```

### Key Components
- `main.rs`: Contains the CLI setup and argument parsing logic.
- `lib.rs`: Implements the `find_matches` function to search for patterns within the content.

#### Sample `lib.rs` Implementation
```rust
pub fn find_matches(content: &str, pattern: &str) {
    for line in content.lines() {
        if line.contains(pattern) {
            println!("{}", line);
        }
    }
}
```

### Writing Tests
Create a `tests` directory with a `cli.rs` file for test cases:
```rust
#[test]
fn find_a_match() {
    let content = "This is a test\nof the emergency broadcast system";
    let pattern = "test";
    find_matches(content, pattern);
}
```

### Running Tests
Execute your test suite using:
```bash
cargo test
```


## Test Output

see screen shot

